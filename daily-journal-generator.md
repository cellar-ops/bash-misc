# Daily Journal Generator with Bash and Vim

A quick and dirty file based journal.

## Mac

Add this to the end of your ~/.bashrc:

```
l() {
    if [ -z "$1" ]; then
	    vim ~/notes/`date +%F`.md
    elif [ ${1:0:1} == "-" ]; then
    	if [ -z "$2" ]; then
    	    vim ~/notes/`date -v-${#1}d +%F`.md
    	else
    	    vim ~/notes/`date -v-${#1}d +%F`_$2.md
    	fi
    elif [ ${1:0:1} == "+" ]; then
    	if [ -z "$2" ]; then
    	    vim ~/notes/`date -v+${#1}d +%F`.md
    	else
    	    vim ~/notes/`date -v+${#1}d +%F`_$2.md
    	fi
    elif [ $1 == "todo" ]; then
            vim ~/notes/todo.md
    elif [ $1 == "last" ]; then
        TEMP_COUNT = 1
        while true; do
            if [ -f ~/notes/`date -v-${TEMP_COUNT}d +%F`.md ]; then
                vim ~/notes/`date -v-${TEMP_COUNT}d +%F`.md
                break
            fi
            TEMP_COUNT=$((TEMP_COUNT + 1))
        done
    
    else
	    vim ~/notes/`date +%F`_$1.md
    fi
}
```

## Linux

Add this slight variation to the end of your ~/.bashrc:

```
l() {
    if [ -z "$1" ]; then
	    vim ~/notes/`date +%F`.md
    elif [ ${1:0:1} == "-" ]; then
    	if [ -z "$2" ]; then
    	    vim ~/notes/`date -d "-${#1} days" +%F`.md
    	else
    	    vim ~/notes/`date -d "-${#1} days" +%F`_$2.md
    	fi
    elif [ ${1:0:1} == "+" ]; then
    	if [ -z "$2" ]; then
    	    vim ~/notes/`date -d "+${#1} days" +%F`.md
    	else
    	    vim ~/notes/`date -d "+${#1} days" +%F`_$2.md
    	fi
    elif [ $1 == "todo" ]; then
            vim ~/notes/todo.md
    elif [ $1 == "last" ]; then
        TEMP_COUNT = 1
        while true; do
            if [ -f ~/notes/`date -d "-${TEMP_COUNT} days" +%F`.md ]; then
                vim ~/notes/`date -d "-${TEMP_COUNT} days" +%F`.md
                break
            fi
            TEMP_COUNT=$((TEMP_COUNT + 1))
        done
    
    else
	    vim ~/notes/`date +%F`_$1.md
    fi
}
```

## Usage

Assuming today is 2019-01-06:

* `l` : Edits ~/notes/2019-01-06.md
* `l+` : Edits ~/notes/2019-01-07.md
* `1---` : Edits ~/notes/2019-01-03.md
* `l backup` : Edits ~/notes/2019-01-06_backup.md
* `+ plan` : Edits ~/notes/2019-01-07_plan.md

Updated to include:
* `l todo`: Edits ~/notes/todo.md
* `l last`: Edits the last written file, walking back a day at a time

Change path and editor as needed.



-H