## Mounting a remote filesystem over SSH

First install sshfs. For Debian/Ubuntu, try:
```
sudo apt install sshfs
```

Now try the below, substituting REMOTE_USER, REMOTE_PATH and LOCAL_PATH
```
sshfs -o idmap=user,allow_other,IdentityFile=/home/$USER/.ssh/id_rsa REMOTE_USER:REMOTE_PATH LOCAL_PATH
```

### Example: Hostmonster

Finally, I wrote a quick bash file named `mount-coolsite.sh`, so I can work on my-cool-site.com. Note I first created an id_rsa file by following instructions at [Hostmonster](https://my.hostmonster.com/hosting/help/ssh-keygen Hostmonster)
```
#!/bin/sh

sshfs -o idmap=user,allow_other,IdentityFile=/home/$USER/.ssh/id_rsa MY_HOSTMONSTER_USERNAME:/home3/coolsite ~/coolsite-fs
```

Note /home3/coolsite is the absolute path on the remote server.

### Misc. Notes

If you're using an IDE, be careful how it tracks file changes. Atom, for example, is greedy and will bog down trying to read every file on the remote system. I've had better luck with VS Code. Of course, there is always Vim.

-H