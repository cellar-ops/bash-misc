# Manjaro quick notes

Update:

```
sudo pacman -SyU
```

Install build-essential equivalent:

```
sudo pacman -Sy base-devel
```

UFW is NOT installed by default (GOOD).

To do the perverse thing and install Edge, first install `yay` (Yet Another Yogurt, a pacman / Go wrapper)

```
sudo pacman -Sy yay
yay -S microsoft-edge-dev
```
