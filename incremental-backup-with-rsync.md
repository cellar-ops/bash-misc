## Incremental Backup with Rsync

I've used `rsync` for backups on a cronjob for some time, but lately have been considering how to do incremental backups. For the curious, these days my backup system is a Raspberry Pi Zero W taped to the side of a 5TB USB drive, hidden away somewhere. That little fella makes a great low-use (read: nightly) NAS.

Anyway, I found [this article this morning](http://www.mikerubel.org/computers/rsync_snapshots/) and am itching to try it out. The script takes the form of:

```
rm -rf backup.3
mv backup.2 backup.3
mv backup.1 backup.2
cp -al backup.0 backup.1
rsync -a --delete source_directory/  backup.0/
```

...using hard links to preserve previous versions without wasting space. There's a later improvement in the doc:

```
mv backup.3 backup.tmp
mv backup.2 backup.3
mv backup.1 backup.2
mv backup.0 backup.1
mv backup.tmp backup.0
cp -al backup.1/. backup.0
rsync -a --delete source_directory/ backup.0/
```

...but I'm still fuzzy on what the explicit differences are between the two before committing.

-H